PREFIX ?= /usr/local
BINDIR ?= $(PREFIX)/bin
DOCDIR ?= $(PREFIX)/share/doc/sshcmd
MANDIR ?= $(PREFIX)/share/man

doc: $(basename $(wildcard doc/*.rst))

clean:

	rm -f doc/sshcmd.1
	rm -f doc/sshwrp.1

install: doc

	install -d '$(DESTDIR)$(BINDIR)'
	install -t '$(DESTDIR)$(BINDIR)'      -m 755 sshcmd
	install -t '$(DESTDIR)$(BINDIR)'      -m 755 sshwrp

	install -d '$(DESTDIR)$(MANDIR)/man1'
	install -t '$(DESTDIR)$(MANDIR)/man1' -m 644 doc/sshcmd.1
	install -t '$(DESTDIR)$(MANDIR)/man1' -m 644 doc/sshwrp.1

	install -d '$(DESTDIR)$(DOCDIR)'
	install -t '$(DESTDIR)$(DOCDIR)'      -m 644 README

uninstall:

	rm -f  '$(BINDIR)/sshcmd'
	rm -f  '$(BINDIR)/sshwrp'
	rm -f  '$(MANDIR)/man1/sshcmd.1'
	rm -f  '$(MANDIR)/man1/sshwrp.1'
	rm -rf '$(DOCDIR)'

doc/%.1: doc/%.1.rst

	rst2man '$<' >'$@'

.PHONY: clean doc install uninstall

# :indentSize=3:tabSize=3:noTabs=false:mode=makefile:maxLineLen=78: