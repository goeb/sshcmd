==============================================================================
sshcmd
==============================================================================

------------------------------------------------------------------------------
SSH command wrapper and validator.
------------------------------------------------------------------------------

:Author:         Stefan Göbel <sshcmd ʇɐ subtype ˙ de >
:Date:           2018/08/23
:Version:        0.1
:Manual section: 1
:Manual group:   User Commands

SYNOPSIS
==============================================================================

**sshcmd** *<command>* *[parameters]*

or

**sshcmd** *[options]* *[-- <command> [parameters]]*

DESCRIPTION
==============================================================================

In short, **sshcmd** may be used to:

*  Encode/decode a command to be started over SSH on the remote system, thus
   avoiding any whitespace/quoting/escaping issues.

*  Restrict the user to one (or more) whitelisted command(s).

Running a command over (Open)SSH means either just specifying it on the
command line, if commands are not restricted by the server, or using a
`ForceCommand` server config setting or a `command=…` directive in an
`authorized_keys` file.

In the first case, the user can run any command available to him, but he has
to take care of any whitespace (and other special characters) in the command
and/or its parameters, because OpenSSH will just pass the whole command line
as one string to the user's shell (i.e. it will run `<shell> -c <command>`,
all parameters joined by spaces).

In the second case, the user is restricted to running the one command
configured - it may be a script running multiple commands, or there may be
multiple keys configured to run different commands. But even then the command
line parameters are fixed (again, a script may for example inspect
`$SSH_ORIGINAL_COMMAND` to determine the parameters, but in that case
whitespace is an issue again…).

**sshcmd** was written to take care of both of these issues. On the client
side, it may be used to unambiguously encode the command and its parameters so
that whitespace is not an issue, and on the server side **sshcmd** may be used
to decode the command, check it against a (regular expression-based) whitelist
of one or more commands (and their parameters) and execute it (if it is valid,
of course).

See the *USAGE EXAMPLES* section below for… well… usage examples.

OPTIONS
==============================================================================

*IMPORTANT:* If any argument of an option starts with `-` or `--`, it must not
be separated from the option by whitespace, it must be separated by an equal
sign! For example, and most likely to be the case, for **sshcmd**'s `--option`
option:

.. code::

   --option=<regexp starting with a dash> or -o=<regexp starting with dash>

And in case you're interested why, and because it's just ridiculous, here's
the link to the `argparse` bug report, opened more than eight (!) years ago:
<https://bugs.python.org/issue9334>.

Command Source
------------------------------------------------------------------------------

The command processed by **sshcmd** may be obtained from different sources.
The default command source depends on the operation, see the next section for
more details. For all operations other than encode **sshcmd** assumes that the
command is base64 encoded. All input sources are available for all operations,
whether it makes sense or not, e.g. it is possible to encode the command read
from the `$SSH_ORIGINAL_COMMAND` environment variable using the respective
options.

The following options select and configure the command source. With the
exception of `--raw`, the following options are mutually exclusive, and
specifying more than one of them will cause an error.

-i, --stdin

   Read the command from standard input (`STDIN`). In this case, the command
   must be split in multiple lines, the first line being the command itself.
   Any command line parameters follow, one line for each parameter. Note that
   for unencoded commands this means that the command or its parameters must
   not contain a newline!

-l, --cmdln

   The command is specified on the **sshcmd** command line, following any
   **sshcmd** options. To avoid ambiguity, the **sshcmd** options and the
   command may be separated by `--`. The first non-option will cause the
   remaining options to be treated as part of the command.

-s, --ssh-env

   The contents of the `$SSH_ORIGINAL_COMMAND` environment variable are split
   at whitespace to get the command and its command line parameters. Note that
   these contents are not processed in any way, so this will in almost all
   cases not work for unencoded commands and/or parameters containing any
   whitespace characters (it also does not work for shell quoted strings
   containing spaces)!

-r, --raw

   Do not try to decode the command. This option does nothing when encoding a
   command. It is mainly useful for testing, to be able to specify the command
   to test on the command line, but it may be used for decode and execute
   mode, too, if required. The option may be used for all command sources.
   Apart from skipping the decoding step everything will work exactly the same
   with or without `--raw`.

Operation
------------------------------------------------------------------------------

The operation parameter determines what **sshcmd** will do with the command
read from the configured command source. The following options are mutually
exclusive.

-e, --encode

   Encode the command. This is the default mode if no other mode is specified.
   The command and its parameters will be base64-encoded, each individually,
   and printed to standard output, one encoded value per line.

   The default command source for this mode is the command line (`--cmdln`).

-x, --execute

   Read and decode the command (unless disabled with `--raw`), try to validate
   it against the configured command(s), and - on success - execute the
   command. The **sshcmd** process will be replaced by the command, the exit
   code will be that of the command. If validation fails an error message will
   be printed to `STDERR` and **sshcmd** will exit with an error. The command
   will be looked up in `$PATH` if necessary.

   The default command source for this mode is standard input (`--stdin`).

   See the *Command Execution Options* below for options that affect the way
   the command is run.

-d, --decode

   Decode the command. This will decode the command and print it to standard
   output (`STDOUT`), command and parameters will be separated by single
   spaces. The command and its parameters will be escaped and/or quoted in
   shell style (i.e. it may be executed with `<shell> -c <output>`). If
   `--raw` is set, the command will be used as read from the source, but shell
   escapes will still be applied.

   The default command source for this mode is standard input (`--stdin`).

-t, --test

   Read and decode the command (unless disabled with `--raw`), then try to
   validate it against the configured command(s). On success, a success
   message will be printed to standard output (`STDOUT`) and **sshcmd** exits
   with no error code. On failure, a failure message will be printed to
   standard error (`STDERR`) and **sshcmd** exits with an error.

   The default command source for this mode is the command line (`--cmdln`).

Validation Options
------------------------------------------------------------------------------

Valid commands are configured by command line options. Multiple commands may
be configured. If no valid command is defined, validation will always fail.

In general, a command looks like this:

.. code::

   <command> <positional parameters> <options> <positional parameters>

All of these parts - except the `<command>` itself are optional. The
`<options>` are defined as

.. code::

   <option> <arguments>

An `<option>` may be followed by an arbitrary number (zero or more) of
`<arguments>`.

If defined, the `<positional parameters>` are mandatory and must be present in
the command's parameters. `<options>` are always optional, and defined options
may appear multple times. See the individual options below for more details.

The validation is based on regular expressions, matching the command and its
parameters individually as they were read from the configured command source.
The command and its parameters will not be modified in any way before the
validation (no path processing, no command line parameter processing…).

See <https://docs.python.org/3/library/re.html> for Python's regular
expression syntax.

Note that by default the full parameters (i.e. the whole string) must match
the regexp, even if this does not include any `^` and/or `$` anchors. See the
`--partial` option for more details on how to change this behaviour.

-c <regexp>, --command <regexp>

   Regular expression matching the command. Each occurrence of the `--command`
   option starts a new command configuration, all other options described in
   this section always apply to the last command defined by `--command`. Note
   that the regular expression must match the command as it is read from the
   command source, i.e. if the command is specified by its full path, the
   regexp must match that full path, if it is a command name only (to be
   looked up in `$PATH`) the regexp must match that command name.

-o <regexp>, --option <regexp>

   Regular expression matching a valid option of the command. The command's
   options are not processed in any way, the regular expression must match the
   options as they are specified. For example, there will be no splitting of
   concatenated single character options. None of the options is considered to
   be required, if a command is called without any options even though some
   are defined as valid options, validation will still succeed if all other
   parameters can be validated. Also, the order of options does not matter,
   and valid options may occur more than once in the command's command line.

   Options may have an arbitrary number of arguments, these can be defined
   using the `--argument` option.

   *Note:* Remember to separate the `--option` and its argument by an equal
   sign if the argument starts with a dash!

-a <regexp>, --argument <regexp>

   Regular expression matching an argument of the previously defined option.
   If one or more arguments are defined for an option, they must be present on
   the command line, in the defined order. An `--option` must be defined
   before an `--argument` can be defined.

-p <regexp>, --parameter <regexp>

   Regular expression matching a positional parameter of the command.

   If the `--command` option is immediately followed by one or more
   `--parameter` options, these parameters must appear on the command's
   command line as the first parameters, all are required, in the correct
   order.

   If one or more `--parameter` options appear after one or more `--option`
   parameters or the `--allow-opts` parameter, these positional parameters
   must be the last parameters on the command's command line, again all must
   be present, in the defined order.

   Both of these positional parameters may be set for a single command, for
   example

   .. code::

      … --command x --parameter p1 --parameter p2 --option opt --parameter p3

   defines a command `x` that will only be validated successfully if the
   parameter list starts with `p1` and `p2` and ends with `p3`. The option
   `--opt` may appear zero or more times between the `p2` and `p3` parameter.

-m, --partial

   Enable partial matching of the regular expressions.

   By default, all regular expressions will be matched against the complete
   command or option/argument/parameter string, i.e. there is no need to
   include the beginning and end anchors (`^` and `$`). Using this option, the
   regular expressions will match anywhere in the string unless an anchor is
   present.

   This setting applies to all regular expressions for the current command!
   Starting a new `--command` will reset it.

-w, --allow-opts

   If this parameter is set, only the command itself and any positional
   parameters specified with `--parameter` will be validated. Any other
   parameters that appear in the command's parameter list will be considered
   valid.

   This setting applies to the current command only. Starting a new
   `--command` will reset it.

Command Execution Options
------------------------------------------------------------------------------

-f <file>, --stdout <file>

   Redirect the command's `STDOUT` to this file. The output will be appended
   to the file if it already exists.

-g <file>, --stderr <file>

   Redirect command's `STDERR` to this file.  The output will be appended to
   the file if it already exists.

-n, --nohup

   Run the command with `nohup`. The command will be validated as usual, and
   on success `nohup` will be executed, with the parameters set to the
   original command and its parameters.

-u, --null

   Redirect the command's `STDOUT` and `STDERR` to `/dev/null`. This option
   will override `--stdout` and `--stderr`.

Miscellaneous Options
------------------------------------------------------------------------------

-b, --debug

   Enable some debug output. Right now this will only enable traces printed in
   case of an error.

-h, --help

   Show **sshcmd**'s help message.

-v, --version

   Show **sshcmd**'s version information.

USAGE EXAMPLES
==============================================================================

Client side, **sshcmd** will just be used to encode the command to be executed
on the server:

.. code::

   sshcmd -- cmd --opt arg param

The `--` are not really required, unless the command name may be mistaken for
an **sshcmd** option. The default mode of **sshcmd** is `--encode`, and in
this mode the default command source is the command line (`--cmdln`), so these
options may be ommitted. The encoded command will be printed to `STDOUT`, with
the encoded command and every individual parameter separated by newlines.

On the server, **sshcmd** will be used to decode, validate and run the
command. To restrict the user to some whitelisted commands, **sshcmd** may be
set as a `ForceCommand` in the server config, or as `command` for selected
keys in the user's `authorized_keys` file. To whitelist the command above, it
may be set to:

.. code::

   sshcmd -x --command cmd --option=--opt --argument arg --parameter param

In `--execute` mode the command will be run after successful validation. The
default command source in `--execute` mode is standard input (`--stdin`). If
the command is set to be executed for every SSH connection for the user as
described above, the user may run it on `example.com` with the following
command (`ssh` options omitted):

.. code::

   sshcmd -- cmd --opt arg param | ssh […] example.com

The **sshcmd** will encode and print the encoded command, output will be piped
to `ssh` which will run **sshcmd** on the server (as listed above), which will
read the encoded command from `STDIN`, validate and run it.

If the user is allowed to run arbitrary commands on the server, **sshcmd** may
be useful to avoid issues with whitespace in the command or its parameters.
Again, it will be run on the client to encode the command, and on the server
to decode and run it:

.. code::

   sshcmd -- cmd "arg with spaces" | ssh […] example.com -- sshcmd -x -c .+ -w

Note that the combination of `-c .+` and `-w` will effectively match every
command and arbitrary parameters.

Let's see a real example. The user will be allowed to run `ip addr show` on
the server, with the options `-h` and `-s` being allowed. Unlike the examples
above, on the server **sshcmd**'s `--ssh-env` option will be set, so the
command will be read from the environment variable `$SSH_ORIGINAL_COMMAND`.
The command to be executed has to be set on the `ssh` command line instead of
being read from a pipe.

We set the `command` in the `authorized_keys` file to:

.. code::

   sshcmd -x --ssh-env -c ip -o=-h -o=-s -p addr -p show

To pass validation, the `ip` command must be called with the two parameters
`addr` and `show` as the last parameters (in that order), the options `-h` and
`-s` if specified must appear before these two parameters. The options are
optional, and order of options does not matter.

With `--ssh-env` set on the server, the encoded command must be specified as
parameter(s) of the `ssh` command on the client. Using POSIX shell syntax:

.. code::

   ssh […] example.com -- $( sshcmd -s ip addr show )

To (locally) test if a command can be validated with some settings, **sshcmd**
supports a `--test` mode. In the following example, a simple call to `ls` will
be tested:

.. code::

   sshcmd --test --raw --command ls --option=-[ahl]+ -- ls -ahl

The `--test` mode uses `--stdin` as default command source, but still assumes
the command is encoded. The `--raw` option disabled the decoding attempt. On
success, the result will be a message on **sshcmd**'s standard output or error
about the success or failure of the validation, respectively. No command will
actually be run in test mode. This example also shows how to allow simple one
letter command line options that don't require an argument. With the command
above, all of the following test will succeed:

.. code::

   sshcmd --test --raw --command ls --option -[ahl]+ -- ls
   sshcmd --test --raw --command ls --option -[ahl]+ -- ls -a -h -l
   sshcmd --test --raw --command ls --option -[ahl]+ -- ls -ahl
   sshcmd --test --raw --command ls --option -[ahl]+ -- ls -ah -l
   sshcmd --test --raw --command ls --option -[ahl]+ -- ls -aahhll

   etc.

SEE ALSO
==============================================================================

*ssh*\ (1), *ssh_config*\ (5), *sshd_config*\ (5), *sshwrp*\ (1)

LICENSE
==============================================================================

Copyright © 2017-2018 Stefan Göbel < sshcmd ʇɐ subtype ˙ de >.

**sshcmd** is free software: you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option) any later
version.

**sshcmd** is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
**sshcmd**. If not, see <http://www.gnu.org/licenses/>.

.. :indentSize=3:tabSize=3:noTabs=true:mode=rest:maxLineLen=78: