==============================================================================
sshwrp
==============================================================================

------------------------------------------------------------------------------
SSH command wrapper and validator - helper program.
------------------------------------------------------------------------------

:Author:         Stefan Göbel <sshcmd ʇɐ subtype ˙ de >
:Date:           2018/08/23
:Version:        0.1
:Manual section: 1
:Manual group:   User Commands

SYNOPSIS
==============================================================================

**sshwrp** *<sshwrp options>* -- *<ssh options>* -- *<command>* *[parameters]*

DESCRIPTION
==============================================================================

**sshwrp** is simple wrapper script to call **sshcmd** and **ssh**, for cases
where shell constructs (pipes, command substitution) can't be used.

**sshwrp** will call **sshcmd** to encode the specified `<command>` and its
`[parameters]`, and pass the resulting encoded command to `ssh`. `ssh` will be
executed with the specified `<ssh options>`. There are two ways to pass the
encoded command: via `ssh`'s `STDIN` (i.e. using a pipe), or on the `ssh`
command line. The mode to be used will be selected by the `<sshwrp options>`.

Note: The double dashes (`--`) separating the different options are required!

OPTIONS
==============================================================================

The following **sshwrp** options match the options that must be set on the
server for the **sshcmd** to select the command source, see *sshcmd*\ (5).

Exactly one of these options must be specified:

-i, --stdin

   Pipe the encoded command to `ssh`.

-s, --ssh-env

   Pass the encoded command to `ssh` on the command line.

The following options are specific to **sshwrp**:

-C <comment>, --comment <comment>

   Does nothing. The `<comment>` argument will be ignored.

EXAMPLES
==============================================================================

Using the examples from **sshcmd**'s man page:

.. code::

   sshcmd -- cmd --opt arg param | ssh […] example.com

   ssh […] example.com -- $( sshcmd -s ip addr show )

Using **sshwrp**, the following commands will do exactly the same,
respectively:

.. code::

   sshwrp --stdin -- […] example.com -- cmd --opt arg param

   sshwrp --ssh-env -- […] example.com -- ip addr show

The following example shows how to use **sshwrp** inside a `.desktop` file to
provide an XFCE preferred application entry for Firefox, running inside the
`container`:

.. code::

   X-XFCE-CommandsWithParameter=sshwrp -i -C %B -- container -- firefox "%s";

(This requires the proper SSH configuration for the `container` host, as well
as modifications to the `X-XFCE-Binaries` in the `.desktop` file to refer to
the proper binary path as seen from the host.)

SEE ALSO
==============================================================================

*ssh*\ (1), *sshcmd*\ (1)

LICENSE
==============================================================================

Copyright © 2017-2018 Stefan Göbel < sshcmd ʇɐ subtype ˙ de >.

**sshcmd** is free software: you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option) any later
version.

**sshcmd** is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
**sshcmd**. If not, see <http://www.gnu.org/licenses/>.

.. :indentSize=3:tabSize=3:noTabs=true:mode=rest:maxLineLen=78: