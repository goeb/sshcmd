#!/bin/bash
#
# Usage: sshwrp <-i|-s> -- <ssh options> -- <command> [<parameters>]
#
# See doc/sshwrp.1.rst (or sshwrp(1) if installed) for more information.
#
##############################################################################

set -o errexit
set -o nounset
set -o pipefail

opts=$( getopt -n 'sshwrp' -o 'isC:' -l 'comment:,stdin,ssh-env' -- "$@" )
eval set -- "$opts"

mode=''
while (( $# > 0 )) ; do
   case "$1" in
      -i | --stdin   ) mode='stdin' ; shift ;;
      -s | --ssh-env ) mode='socmd' ; shift ;;
      -C | --comment ) shift        ; shift ;;
      --             ) shift        ; break ;;
   esac
done

if [[ -z "$mode" ]] ; then
   printf 'Error: One of -i/--stdin or -s/--ssh-env is required.\n' >&2
   exit 1
fi

args=()
while (( $# > 0 )) ; do
   case "$1" in
      -- ) shift          ; break ;;
      *  ) args+=( "$1" ) ; shift ;;
   esac
done

if (( ${#args[@]} == 0 )) ; then
   printf 'Error: No SSH options specified.\n' >&2
   exit 1
fi

if (( $# == 0 )) ; then
   printf 'Error: No command specified.\n' >&2
   exit 1
fi

case "$mode" in
   stdin ) sshcmd -- "$@" | ssh "${args[@]}"       ;;
   socmd ) ssh "${args[@]}" -- $( sshcmd -- "$@" ) ;;
esac

#:indentSize=3:tabSize=3:noTabs=true:mode=shellscript:maxLineLen=78:##########